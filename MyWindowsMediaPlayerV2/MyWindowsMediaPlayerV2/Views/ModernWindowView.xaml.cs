﻿using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;
using MyWindowsMediaPlayerV2.Model;

namespace MyWindowsMediaPlayerV2.Views
{
    public partial class ModernWindowView : ModernWindow
    {
        public ModernWindowView()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            CollectionManager.Instance().Library.saveCollection();
            CollectionManager.Instance().Playlists.saveCollection();
        }
    }
}
