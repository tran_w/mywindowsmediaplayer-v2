﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyWindowsMediaPlayerV2.Content
{
    public partial class ContentImageLibrary : UserControl
    {
        public enum Filetype { jpeg, bmp, gif };
        public class Image
        {

            #region Properties
            public string Title { get; set; }
            public string Size { get; set; }
            public DateTime CreatedAt { get; set; }
            public bool IsAllowedToBePlayed { get; set; }
            public Filetype Type { get; set; }
            #endregion // Properties

        }

        public ContentImageLibrary()
        {
            InitializeComponent();
        }

        private ObservableCollection<Image> GetData()
        {
            return null;
        }
    }
}
