﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayerV2.Model
{
    [XmlType(TypeName = "Picture")]
    [Serializable]
    public class PictureModel : MediaModel
    {
        #region Ctor & Dtor
        public PictureModel() : base() { }
        public PictureModel(string title = "", string path = "", long size = 0,
           string author = "",
           DateTime added = default(DateTime))
            : base(title, path, size, author, added) { }
        #endregion // Ctor & Dtor
    }
}
