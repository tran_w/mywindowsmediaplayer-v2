﻿using Microsoft.Win32;
using MyWindowsMediaPlayerV2.Helper_classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace MyWindowsMediaPlayerV2.Model
{
    class CurrentMedia : ObservableObject
    {

        #region Private
        private MediaElement _media;
        private static CurrentMedia _instance = null;
        #endregion // Private

        #region Ctor
        private CurrentMedia()
        {
            _media = new MediaElement();
        }
        #endregion // Ctor

        #region Public
        public static CurrentMedia Instance()
        {
            if (_instance == null)
                _instance = new CurrentMedia();
            return _instance;
        }

        public Uri Source
        {
            get
            {
                return Media.Source;
            }
            set
            {
                Media.Source = value;
                OnPropertyChanged("Source");
            }
        }
        public MediaElement Media { get { return _media; } set { _media = value; } }
        #endregion // Public

    }
}
