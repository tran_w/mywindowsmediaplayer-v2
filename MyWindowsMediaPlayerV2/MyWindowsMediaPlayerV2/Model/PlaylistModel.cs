﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Serialization;
using MusicList = System.Collections.ObjectModel.ObservableCollection<MyWindowsMediaPlayerV2.Model.MusicModel>;

namespace MyWindowsMediaPlayerV2.Model
{
    [Serializable]
    public class PlaylistModel
    {
        #region Fields
        [XmlElement("PlaylistName")]
        public string _name { get; set; }
        [XmlArray("MusicList")]
        public MusicList _musicList { get; set; }
        #endregion //Fields

        #region Ctor & Dtor
        public PlaylistModel()
        {
            _name = "";
            _musicList = new MusicList();
        }

        public PlaylistModel(string name = null, MusicList oldMusicList = null)
        {
            _name = (name == null ? "" : name);
            _musicList = (oldMusicList == null ? new MusicList() : oldMusicList);
        }

        #endregion // Ctor & Dtor

    }
}
