﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayerV2.Model
{
    [XmlType(TypeName = "Media")]
    [Serializable]
    public class MediaModel
    {

        #region Fields
        private string _title;
        private string _path;
        private long _size;
        private DateTime _added;
        private string _author;
        #endregion // Fields

        #region Properties
        [XmlElement("Title")]
        public String Title { get { return _title; } set { _title = value; } }
        [XmlElement("Path")]
        public String Path { get { return _path; } set { _path = value; } }
        [XmlElement("Size")]
        public long Size { get { return _size; } set { _size = value; } }
        [XmlElement("AddOn")]
        public DateTime Added { get { return _added; } set { _added = value; } }
        [XmlElement("Author")]
        public String Author { get { return _author; } set { _author = value; } }
        #endregion // Properties

        #region Ctor & Dtor
        public MediaModel()
        {
            _title = "";
            _path = "";
            _size = 0;
            _added = DateTime.MinValue;
            _author = "";
        }

        public MediaModel(string title = "", string path = "", long size = 0,
             string author = "", DateTime added = default(DateTime))
        {
            _title = title;
            _path = path;
            _size = size;
            if (added == default(DateTime))
                _added = DateTime.UtcNow;
            else
                _added = added;
            _author = author;
        }
        #endregion // Ctor & Dtor

    };
}
