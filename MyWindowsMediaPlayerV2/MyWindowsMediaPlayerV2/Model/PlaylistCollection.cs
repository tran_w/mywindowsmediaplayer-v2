﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Serialization;
using PlaylistList = System.Collections.ObjectModel.ObservableCollection<MyWindowsMediaPlayerV2.Model.PlaylistModel>;

namespace MyWindowsMediaPlayerV2.Model
{
    [Serializable]
    public class PlaylistCollection : IMediaCollection
    {
        #region Fields
        [XmlArray("Playlists")]
        public PlaylistList _playlists { get; set; }
        [XmlElement("CurrentPlaylistNb")]
        public int _currPlaylistNb { get; set; }
        #endregion // Fields

        #region Ctor & Dtor
        public PlaylistCollection()
        {
            _playlists = new PlaylistList();
            _currPlaylistNb = 1;
        }

        public PlaylistCollection(PlaylistList oldPlaylists = null)
        {
            _playlists = (oldPlaylists == null ? new PlaylistList() : oldPlaylists);
        }

        #endregion // Ctor & Dtor

        #region Serialization tools
        public void saveCollection(string path = null)
        {
            try
            {
                FileStream file = File.Open((path == null ? System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2\\playlists.xml" : path), FileMode.Create);
                XmlSerializerNamespaces nameSpaceSerializer = new XmlSerializerNamespaces();
                nameSpaceSerializer.Add("", "");
                XmlSerializer serializer = new XmlSerializer(typeof(PlaylistCollection));

                serializer.Serialize(file, this, nameSpaceSerializer);
                file.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: Could not serialize to file " + (path == null ? "playlists.xml" : path) + ". Original error: " + e.Message);            }
        }

        public void loadCollection(string path = null)
        {
            try
            {
                if (!Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2"))
                    Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2");
                if (!File.Exists(path == null ? System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2\\playlists.xml" : path))
                    File.Open((path == null ? System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2\\playlists.xml" : path), FileMode.Create);
                FileInfo f = new FileInfo(path == null ? System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2\\playlists.xml" : path);
                if (f.Length != 0)
                {
                    FileStream file = File.Open((path == null ? System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2\\playlists.xml" : path), FileMode.Open);
                    XmlSerializer serializer = new XmlSerializer(typeof(PlaylistCollection));
                    PlaylistCollection collection = (PlaylistCollection)serializer.Deserialize(file);

                    _playlists = collection._playlists;
                    _currPlaylistNb = collection._currPlaylistNb;
                    file.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: Could not deserialize from file " + (path == null ? "playlists.xml" : path) + ". Original error: " + e.Message);
            }
        }
        #endregion // Serialization tools

    }
}
