﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using Microsoft.Win32;
using System.Windows.Threading;
using System.Threading;
using MyWindowsMediaPlayerV2.Model;

namespace MyWindowsMediaPlayerV2.Model
{
    public class MediaManager
    {

        #region Const values
        public static readonly List<string> ImageExtensions = new List<string> { ".JPG" };
        public static readonly List<string> VideoExtensions = new List<string> { ".MPEG", ".AVI", ".WMV" };
        public static readonly List<string> SoundExtensions = new List<string> { ".MP3", ".WAV" };
        #endregion // Const values

        #region Public
        public void openFile()
        {
            Stream checkStream = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All Supported File Types(*.mp3,*.wav,*.mpeg,*.wmv,*.avi,*.jpg)|*.mp3;*.wav;*.mpeg;*.wmv;*.avi;*.jpg";
            // Show open file dialog box 
            if ((bool)dlg.ShowDialog())
            {
                try
                {
                    if ((checkStream = dlg.OpenFile()) != null)
                    {
                        CurrentMedia.Instance().Source = new Uri(dlg.FileName);
                    }
                    string path = Path.GetExtension(dlg.FileName);
                    if (ImageExtensions.Contains(path.ToUpperInvariant()))
                    {
                        TagLib.File tagFile = TagLib.File.Create(dlg.FileName);
                        FileInfo file = new FileInfo(dlg.FileName);
                        PictureModel New = new PictureModel(Path.GetFileNameWithoutExtension(dlg.FileName), dlg.FileName,
                            file.Length, tagFile.Tag.FirstAlbumArtist, DateTime.UtcNow);

                        if (CollectionManager.Instance().Library._pictureList.Any(p => p.Path == New.Path) == false)
                            CollectionManager.Instance().Library._pictureList.Add(New);
                    }
                    else if (VideoExtensions.Contains(path.ToUpperInvariant()))
                    {
                        TagLib.File tagFile = TagLib.File.Create(dlg.FileName);
                        FileInfo file = new FileInfo(dlg.FileName);
                        VideoModel New = new VideoModel(Path.GetFileNameWithoutExtension(dlg.FileName), dlg.FileName,
                            file.Length, tagFile.Tag.FirstAlbumArtist, DateTime.UtcNow, new TimeSpan(tagFile.Properties.Duration.Ticks));
                        
                        if (CollectionManager.Instance().Library._videoList.Any(p => p.Path == New.Path) == false)
                            CollectionManager.Instance().Library._videoList.Add(New);
                    }
                    else if (SoundExtensions.Contains(path.ToUpperInvariant()))
                    {
                        TagLib.File tagFile = TagLib.File.Create(dlg.FileName);
                        FileInfo file = new FileInfo(dlg.FileName);
                        MusicModel New = new MusicModel(Path.GetFileNameWithoutExtension(dlg.FileName), dlg.FileName,
                            file.Length, tagFile.Tag.FirstAlbumArtist, DateTime.UtcNow, new TimeSpan(tagFile.Properties.Duration.Ticks),
                            true, tagFile.Tag.Album);

                        if (CollectionManager.Instance().Library._musicList.Any(p => p.Path == New.Path) == false)
                            CollectionManager.Instance().Library._musicList.Add(New);
                    }
                    CurrentMedia.Instance().Media.LoadedBehavior = MediaState.Play;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        public void playFile()
        {
            CurrentMedia.Instance().Media.LoadedBehavior = MediaState.Play;
        }

        public void stopFile()
        {
            CurrentMedia.Instance().Media.LoadedBehavior = MediaState.Stop;
        }

        public void pauseFile()
        {
            CurrentMedia.Instance().Media.LoadedBehavior = MediaState.Pause;
        }
        #endregion // Public

        #region Ctor
        public MediaManager()
        {
            LibraryModel library = CollectionManager.Instance().Library;
            PlaylistCollection playlists = CollectionManager.Instance().Playlists;

            library.loadCollection();
            playlists.loadCollection();
        }
        #endregion // Ctor
    }
}
