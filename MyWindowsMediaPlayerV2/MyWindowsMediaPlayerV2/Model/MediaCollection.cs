﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using PictureList = System.Collections.ObjectModel.ObservableCollection<MyWindowsMediaPlayerV2.Model.PictureModel>;
using MusicList = System.Collections.ObjectModel.ObservableCollection<MyWindowsMediaPlayerV2.Model.MusicModel>;
using VideoList = System.Collections.ObjectModel.ObservableCollection<MyWindowsMediaPlayerV2.Model.VideoModel>;

namespace MyWindowsMediaPlayerV2.Model
{
    public class MediaCollection
    {
        #region Fields
        [XmlArray("PictureList")]
        public PictureList pictureList { get; set; }
        [XmlArray("MusicList")]
        public MusicList musicList { get; set; }
        [XmlArray("VideoList")]
        public VideoList videoList { get; set; }
        public PictureModel pictureModel { get; set; }
        #endregion //Fields

        #region Ctor & Dtor
        public string Title
        {
            get
            {
                return pictureModel._title;
            }

        }
        public MediaCollection()
        {
            pictureList = new PictureList();
            /**/
        }

        public MediaCollection(PictureList oldPictureList = null, MusicList oldMusicList = null, VideoList oldVideoList = null)
        {
            pictureList = oldPictureList;
            musicList = oldMusicList;
            videoList = oldVideoList;
        }

        ~MediaCollection()
        {
            /*pictureList.Clear();
            pictureList.Clear();
            pictureList.Clear();*/
        }
        #endregion // Ctor & Dtor

        #region Serialization tools
        public void saveList(string filename)
        {
            FileStream file = File.Open(filename, FileMode.Create);
            XmlSerializerNamespaces nameSpaceSerializer = new XmlSerializerNamespaces();
            nameSpaceSerializer.Add("", "");
            XmlSerializer serializer = new XmlSerializer(typeof(MediaCollection));

            serializer.Serialize(file, this, nameSpaceSerializer);
            file.Close();
        }

        public void loadList(string filename)
        {
            FileStream file = File.Open(filename, FileMode.Open);
            XmlSerializer serializer = new XmlSerializer(typeof(MediaCollection));
            MediaCollection collection = (MediaCollection)serializer.Deserialize(file);
            pictureList = collection.pictureList;
            musicList = collection.musicList;
            videoList = collection.videoList;
            file.Close();
        }
        #endregion // Serialization tools

    };
}
