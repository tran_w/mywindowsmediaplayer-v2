﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayerV2.Model
{
    [XmlType(TypeName = "Music")]
    [Serializable]
    public class MusicModel : MediaModel
    {

        #region Fields
        private TimeSpan _length;
        private Boolean _playable;
        private string _album;
        #endregion // Fields

        #region Properties
        [XmlElement("Length")]
        public long LengthTicks { get { return _length.Ticks; } set { _length = new TimeSpan(value); } }
        [XmlIgnore()]
        public string SLength { get { return _length.ToString("hh':'mm':'ss"); } }
        [XmlIgnore()]
        public TimeSpan Length { get { return _length; } set { _length = value; } }
        [XmlElement("Playable")]
        public Boolean Playable { get { return _playable; } set { _playable = value; } }
        [XmlElement("Album")]
        public string Album { get { return _album; } set { _album = value; } }
        #endregion // Properties

        #region Ctor & Dtor
        public MusicModel() : base()
        {
            _length = default(TimeSpan);
        }

        public MusicModel(string title = "", string path = "", long size = 0,
            string author = "",
            DateTime added = default(DateTime), TimeSpan length = default(TimeSpan),
            Boolean playable = false, string album = "") : base(title, path, size, author, added)
        {
            if (length == default(TimeSpan))
                length = TimeSpan.Zero;
            else
                _length = length;
            _playable = playable;
            _album = album;
        }

        #endregion // Ctor & Dtor

    }
}
