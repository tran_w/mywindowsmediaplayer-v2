﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using PictureList = System.Collections.ObjectModel.ObservableCollection<MyWindowsMediaPlayerV2.Model.PictureModel>;
using MusicList = System.Collections.ObjectModel.ObservableCollection<MyWindowsMediaPlayerV2.Model.MusicModel>;
using VideoList = System.Collections.ObjectModel.ObservableCollection<MyWindowsMediaPlayerV2.Model.VideoModel>;
using System.Windows;
using System.Collections.Generic;
using Microsoft.Win32;

namespace MyWindowsMediaPlayerV2.Model
{
    [Serializable]
    public class LibraryModel : IMediaCollection
    {
        public static readonly List<string> SoundExtensions = new List<string> { ".MP3", ".WAV" };
        #region Fields
        [XmlArray("PictureList")]
        public PictureList _pictureList { get; set; }
        [XmlArray("MusicList")]
        public MusicList _musicList { get; set; }
        [XmlArray("VideoList")]
        public VideoList _videoList { get; set; }
        [XmlElement("Path")]
        public string _path { get; set; }
        #endregion //Fields

        #region Ctor & Dtor
        public LibraryModel()
        {
            _pictureList = new PictureList();
            _musicList = new MusicList();
            _videoList = new VideoList();
            _path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2\\library.xml";
        }

        public LibraryModel(PictureList oldPictureList = null, MusicList oldMusicList = null, VideoList oldVideoList = null, string path = null)
        {
            _pictureList = (oldPictureList == null ? new PictureList() : oldPictureList);
            _musicList = (oldMusicList == null ? new MusicList() : oldMusicList);
            _videoList = (oldVideoList == null ? new VideoList() : oldVideoList);
            _path = (path == null ? System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2\\library.xml" : path);
        }

        public void addSoundToPlaylist(MusicList _currPlaylist)
        {
            Stream checkStream = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All Supported File Types(*.mp3,*.wav)|*.mp3;*.wav";

            if ((bool)dlg.ShowDialog())
            {
                try
                {
                    if ((checkStream = dlg.OpenFile()) != null)
                    {
                        string path = Path.GetExtension(dlg.FileName);
                        if (SoundExtensions.Contains(path.ToUpperInvariant()))
                        {
                            TagLib.File tagFile = TagLib.File.Create(dlg.FileName);
                            FileInfo file = new FileInfo(dlg.FileName);
                            MusicModel New = new MusicModel(Path.GetFileNameWithoutExtension(dlg.FileName), dlg.FileName,
                                file.Length, tagFile.Tag.FirstAlbumArtist, DateTime.UtcNow, new TimeSpan(tagFile.Properties.Duration.Ticks),
                                true, tagFile.Tag.Album);

                            if (_currPlaylist.Any(p => p.Path == New.Path) == false)
                                _currPlaylist.Add(New);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        #endregion // Ctor & Dtor

        #region Serialization tools
        public void saveCollection(string path = null)
        {
            try
            {
                FileStream file = File.Open((path == null ? _path : path), FileMode.Create);
                XmlSerializerNamespaces nameSpaceSerializer = new XmlSerializerNamespaces();
                nameSpaceSerializer.Add("", "");
                XmlSerializer serializer = new XmlSerializer(typeof(LibraryModel));
                serializer.Serialize(file, this, nameSpaceSerializer);
                file.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: Could not serialize to file " + (path == null ? _path : path) + ". Original error: " + e.Message);
            }
        }

        public void loadCollection(string path = null)
        {
            try
            {
                if (!Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2"))
                    Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\MWMPV2");
                if (!File.Exists(path == null ? _path : path))
                    File.Open((path == null ? _path : path), FileMode.Create);
                FileInfo f = new FileInfo((path == null ? _path : path));
                if (f.Length != 0)
                {
                    FileStream file = File.Open((path == null ? _path : path), FileMode.Open);
                    XmlSerializer serializer = new XmlSerializer(typeof(LibraryModel));
                    LibraryModel collection = (LibraryModel)serializer.Deserialize(file);
                    //_pictureList = collection._pictureList;
                    //_musicList = collection._musicList;
                    // _videoList = collection._videoList;
                    if (_pictureList == null || _pictureList.Count == 0)
                    {
                        foreach (PictureModel picture in collection._pictureList)
                        {
                            CollectionManager.Instance().Library._pictureList.Add(picture);
                        }
                    }

                    if (_musicList == null || _musicList.Count == 0)
                    {
                        foreach (MusicModel music in collection._musicList)
                        {
                            CollectionManager.Instance().Library._musicList.Add(music);
                        }
                    }

                    if (_videoList == null || _videoList.Count == 0)
                    {
                        foreach (VideoModel video in collection._videoList)
                        {
                            CollectionManager.Instance().Library._videoList.Add(video);
                        }
                    }
                    file.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: Could not deserialize from file " + (path == null ? _path : path) + ". Original error: " + e.Message);
            }
        }
        #endregion // Serialization tools
    };
}
