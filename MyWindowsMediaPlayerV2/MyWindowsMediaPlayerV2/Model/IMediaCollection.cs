﻿using MyWindowsMediaPlayerV2.Helper_classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWindowsMediaPlayerV2.Model
{
    interface IMediaCollection
    {
        #region IMethods
        void saveCollection(string path);
        void loadCollection(string path);
        #endregion // IMethods
    }
}
