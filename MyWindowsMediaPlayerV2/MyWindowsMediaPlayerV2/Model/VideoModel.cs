﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayerV2.Model
{
    [XmlType(TypeName = "Video")]
    [Serializable]
    public class VideoModel : MediaModel
    {

        #region Fields
        private TimeSpan _length;
        #endregion // Fields

        #region Properties
        [XmlIgnore()]
        public string SLength { get { return _length.ToString("hh':'mm':'ss"); } }
        [XmlIgnore()]
        public TimeSpan Length { get { return _length; } set { _length = value; } }
        [XmlElement("Length")]
        public long LengthTicks { get { return _length.Ticks; } set { _length = new TimeSpan(value); } }
        #endregion // Properties

        #region Ctor & Dtor
        public VideoModel() : base()
        {
            _length = default(TimeSpan);
        }

        public VideoModel(string title = "", string path = "", long size = 0,
            string author = "",
            DateTime added = default(DateTime), TimeSpan length = default(TimeSpan)) : base(title, path, size, author, added)
        {
            if (length == default(TimeSpan))
                length = TimeSpan.Zero;
            else
            {
                _length = length;
            }
        }

        #endregion // Ctor & Dtor

    };
}
