﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MyWindowsMediaPlayerV2.Model
{
    class CollectionManager
    {

        #region Private
        private LibraryModel _library;
        private PlaylistCollection _playlists;
        private static CollectionManager _instance = null;
        #endregion // Private

        #region Ctor
        private CollectionManager()
        {
            _library = new LibraryModel();
            _playlists = new PlaylistCollection();
        }
        #endregion // Ctor

        #region Public
        public static CollectionManager Instance()
        {
            if (_instance == null)
                _instance = new CollectionManager();
            return _instance;
        }

        public LibraryModel Library { get { return _library; } set { _library = value; } }
        public PlaylistCollection Playlists { get { return _playlists; } set { _playlists = value; } }
        #endregion // Public

    }
}
