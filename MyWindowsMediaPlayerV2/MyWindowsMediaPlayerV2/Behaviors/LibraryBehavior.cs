﻿using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace MyWindowsMediaPlayerV2.Behaviors
{
    class LibraryBehavior : Behavior<Button>
    {
        public static readonly DependencyProperty SelectedItemsProperty =
        DependencyProperty.Register("SelectedItems", typeof(string), typeof(LibraryBehavior), new PropertyMetadata(default(string), OnSelectedItemsChanged));

        #region Behaviour
        private static void OnSelectedItemsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var grid = ((LibraryBehavior)sender).AssociatedObject;
            if (grid == null) return;
        }

        public string SelectedItems
        {
            get { return (string)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Click += AssociatedObject_Click;
        }

        private void AssociatedObject_Click(object sender, RoutedEventArgs e)
        {
            SelectedItems = ((Button)sender).Content.ToString();
        }
        #endregion // Behaviour
    }
}
