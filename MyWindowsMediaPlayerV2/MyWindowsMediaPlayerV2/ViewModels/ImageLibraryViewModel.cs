﻿using Microsoft.Win32;
using MyWindowsMediaPlayerV2.Helper_classes;
using MyWindowsMediaPlayerV2.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections;

namespace MyWindowsMediaPlayerV2
{
    class ImageLibraryViewModel : ObservableObject
    {
        #region Fields
        private IList _selectedImages;
        #endregion // Fields

        #region Public Properties/Commands
        public ObservableCollection<PictureModel> getImageLibrary
        {
            get
            {
                return CollectionManager.Instance().Library._pictureList;
/*                if (_library == null)
                {
                    _library = new LibraryModel();
                    _library.loadCollection();
                }
                return _library._pictureList;*/
            }
         }

        public IList SelectedImages
        {
            get { return _selectedImages; }
            set
            {
                _selectedImages = value;
                if (_selectedImages != null)
                {
                    if (File.Exists(((PictureModel)_selectedImages[0]).Path))
                    {
                        CurrentMedia.Instance().Source = new Uri(((PictureModel)_selectedImages[0]).Path);
                        Console.WriteLine(CurrentMedia.Instance().Source.ToString());
                    }
                }
            }
        }

        #endregion // Public Properties/Commands
    }
}
