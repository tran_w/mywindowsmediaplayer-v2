﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using MyWindowsMediaPlayerV2.Model;
using MyWindowsMediaPlayerV2.Helper_classes;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using Microsoft.Win32;
using System.Windows.Controls.Primitives;
using Caliburn.Micro;

namespace MyWindowsMediaPlayerV2.ViewModels
{
    public class HomeViewModel : ObservableObject
    {
        #region Fields
        private ICommand _openFileCommand;
        private ICommand _playFileCommand;
        private ICommand _stopFileCommand;
        private ICommand _pauseFileCommand;
        private MediaManager _mediaManager;
        #endregion // Fields

        #region Public Properties/Commands

        public ObservableObject CurrentMediaElement
        {
            get
            {
                return CurrentMedia.Instance();
            }
        }

        public MediaManager MediaManager
        {
            get
            {
                return _mediaManager;
            }
        }

        public ICommand PlayFileCommand
        {
            get
            {
                if (_playFileCommand == null)
                {
                    _playFileCommand = new RelayCommand(
                        param => PlayFile()
                        );
                }
                return _playFileCommand;
            }
        }

        public ICommand OpenFileCommand
        {
            get
            {
                if (_openFileCommand == null)
                {
                    _openFileCommand = new RelayCommand(
                        param => OpenFile()
                        );
                }
                return _openFileCommand;
            }
        }

        public ICommand StopFileCommand
        {
            get
            {
                if (_stopFileCommand == null)
                {
                    _stopFileCommand = new RelayCommand(
                        param => StopFile()
                        );
                }
                return _stopFileCommand;
            }
        }

        public ICommand PauseFileCommand
        {
            get
            {
                if (_pauseFileCommand == null)
                {
                    _pauseFileCommand = new RelayCommand(
                        param => PauseFile()
                        );
                }
                return _pauseFileCommand;
            }
        }
        #endregion // Public Properties/Commands

        #region Private Helpers

        private void OpenFile()
        {
            MediaManager.openFile();
        }

        private void PlayFile()
        {
            MediaManager.playFile();
        }

        private void StopFile()
        {
            MediaManager.stopFile();
        }
        private void PauseFile()
        {
            MediaManager.pauseFile();
        }

        #endregion // Private Helpers

        #region Ctor & Dtor
        public HomeViewModel()
        {
            _mediaManager = new MediaManager();
        }
        #endregion // Ctor & Detor
     }
}
