﻿using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using MyWindowsMediaPlayerV2.Helper_classes;
using MyWindowsMediaPlayerV2.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MyWindowsMediaPlayerV2
{
    class PlaylistLibraryViewModel : ObservableObject
    {
        #region Fields
        private string _selectedPlaylist = "Library";
        private ICommand _addPlaylistCommand;
        private Link _libraryLink = new Link
        {
            DisplayName = "Library"
        };
        private ICommand _openFileCommand;
        private LinkCollection _links = new LinkCollection();
        private IList _selectedMusic;
        private ObservableCollection<MusicModel> _filteredPlaylist = new ObservableCollection<MusicModel>();
        private ObservableCollection<MusicModel> _currPlaylist = new ObservableCollection<MusicModel>();
        private ObservableCollection<MusicModel> _tmpPlaylist = new ObservableCollection<MusicModel>();
        private ObservableCollection<MusicModel> _clonePlaylist = new ObservableCollection<MusicModel>();

        private string _filterText;
        private ICommand _searchMusicCommand;
        #endregion // Fields

        #region Public Properties/Commands

        public ICommand OpenFileCommand
        {
            get
            {
                if (_openFileCommand == null)
                {
                    _openFileCommand = new MyWindowsMediaPlayerV2.Helper_classes.RelayCommand(
                        param => OpenFile()
                        );
                }
                return _openFileCommand;
            }
        }

        public PlaylistLibraryViewModel()
        {
            _links.Add(_libraryLink);
            foreach (MusicModel music in CollectionManager.Instance().Library._musicList)
                _filteredPlaylist.Add(music);

            ObservableCollection<PlaylistModel> playlists = CollectionManager.Instance().Playlists._playlists;
            foreach (var playlist in playlists)
            {
                _links.Add(new Link
                {
                    DisplayName = playlist._name
                });
            }
            foreach (var element in CollectionManager.Instance().Library._musicList)
            {
                _currPlaylist.Add(element);
            }
        }

        public ObservableCollection<MusicModel> GetPlaylist
        {
            get
            {
                if (_selectedPlaylist == "Library")
                    return CollectionManager.Instance().Library._musicList;
                return _currPlaylist;
            }
        }

        public LinkCollection PlaylistLinks
        {
            get
            {
                return _links;
            }
        }

        public ICommand AddPlaylistCommand
        {
            get
            {
                if (_addPlaylistCommand == null)
                    _addPlaylistCommand = new Helper_classes.RelayCommand(
                        param => AddPlaylist());
                return _addPlaylistCommand;
            }
        }

        public IList SelectedMusic
        {
            get { return _selectedMusic; }
            set
            {

                _selectedMusic = value;
                OnPropertyChanged("SelectedMusic");
                if (_selectedMusic != null)
                {
                    if (_selectedMusic.Count != 0)
                        CurrentMedia.Instance().Source = new Uri(((MusicModel)_selectedMusic[0]).Path);
                }
            }
        }

        public string SelectedPlaylist
        {
            get
            {
                return _selectedPlaylist;
            }
            set
            {
                string tmp = _selectedPlaylist;
                _selectedPlaylist = value;
                OnPropertyChanged("GetPlaylist");

                if (_selectedPlaylist != null && _selectedPlaylist != "Library")
                {
                    var playlistList = from list in CollectionManager.Instance().Playlists._playlists
                                       where list._name == _selectedPlaylist
                                       select list;
                    _currPlaylist.Clear();
                    foreach (MusicModel element in playlistList.First()._musicList)
                    {
                        _currPlaylist.Add(element);
                    }
                }
                else
                {
                    _currPlaylist.Clear();
                    foreach (MusicModel element in CollectionManager.Instance().Library._musicList)
                    {
                        _currPlaylist.Add(element);
                    }
                }
            }
        }

        public string FilterText
        {
            get
            {
                return _filterText;
            }
            set
            {
                if (_filterText != value)
                {
                    _filterText = value;
                    OnPropertyChanged("FilterText");
                }
            }
        }

        public ICommand SearchMusicCommand
        {
            get
            {
                if (_searchMusicCommand == null)
                    _searchMusicCommand = new Helper_classes.RelayCommand(
                        param => SearchMusic());
                return _searchMusicCommand;
            }
        }
        //public 
        #endregion // Public Properties/Commands

        #region Private Fields
        private void OpenFile()
        {
            foreach (var playlist in CollectionManager.Instance().Playlists._playlists)
            {
                if (playlist._name == _selectedPlaylist)
                {
                    CollectionManager.Instance().Library.addSoundToPlaylist(playlist._musicList);
                    _currPlaylist.Clear();
                    foreach (var element in playlist._musicList)
                    {
                        if (CollectionManager.Instance().Library._musicList.Contains(element) == false)
                            CollectionManager.Instance().Library._musicList.Add(element);
                        _currPlaylist.Add(element);
                    }
                }
            }
        }

        private void AddPlaylist()
        {
            CollectionManager.Instance().Playlists._playlists.Add(new PlaylistModel("Playlist" + CollectionManager.Instance().Playlists._currPlaylistNb, null));
            _links.Add(new Link
                {
                    DisplayName = "Playlist" + CollectionManager.Instance().Playlists._currPlaylistNb++
                });
        }

        private void SearchMusic()
        {
        }
        //private
        #endregion // Private Fields
    }
}
