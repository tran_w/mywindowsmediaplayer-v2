﻿using MyWindowsMediaPlayerV2.Helper_classes;
using MyWindowsMediaPlayerV2.Model;
using MyWindowsMediaPlayerV2.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Serialization;

namespace MyWindowsMediaPlayerV2.ViewModels
{
    class VideoLibraryViewModel : ObservableObject
    {
        #region Fields
        private IList _selectedCultures;
        #endregion // Fields

        #region Public Properties/Commands

        public ObservableCollection<VideoModel>  getVideoLibrary
        {
            get
            {
                return CollectionManager.Instance().Library._videoList;
            }
        }

        public IList SelectedCultures
        {
            get { return _selectedCultures; }
            set
            {

                _selectedCultures = value;
                OnPropertyChanged("SelectedCultures");
                if (_selectedCultures != null)
                {
                    if (File.Exists(((VideoModel)_selectedCultures[0]).Path))
                        CurrentMedia.Instance().Source = new Uri(((VideoModel)_selectedCultures[0]).Path);
                }
            }
        }

        #endregion // Public Properties/Commands
    }
}
